<?php

// Any Post Widget
class BA_Sales_Widget extends WP_Widget {


    function BA_Sales_Widget() {
        $widget_ops = array('classname' => 'ba-saleswidget', 'description' => 'Shows sales from devs' );
        $this->WP_Widget('BA_Sales_Widget', 'PageLines DevSales', $widget_ops);
    }


    function form($instance) {
        $defaults = array(
            'title' => '',
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
        $title = esc_attr($instance['title']);
        $user = esc_attr($instance['devsales_user']);
        $apikey = esc_attr($instance['devsales_apikey']);
        $product = esc_attr($instance['devsales_product']);
        $days = esc_attr($instance['devsales_days']);

        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('devsales_apikey'); ?>">API Key</label>
            <input class="widefat" id="<?php echo $this->get_field_id('devsales_apikey'); ?>" name="<?php echo $this->get_field_name('devsales_apikey'); ?>" type="text" value="<?php echo esc_attr($apikey); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('devsales_user'); ?>">User</label>
            <input class="widefat" id="<?php echo $this->get_field_id('devsales_user'); ?>" name="<?php echo $this->get_field_name('devsales_user'); ?>" type="text" value="<?php echo esc_attr($user); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('devsales_days'); ?>">Go Back X Days</label>
            <input class="widefat" id="<?php echo $this->get_field_id('devsales_days'); ?>" name="<?php echo $this->get_field_name('devsales_days'); ?>" type="text" value="<?php echo esc_attr($days); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('devsales_product'); ?>">Product</label>
            <input class="widefat" id="<?php echo $this->get_field_id('devsales_product'); ?>" name="<?php echo $this->get_field_name('devsales_product'); ?>" type="text" value="<?php echo esc_attr($product); ?>" />
        </p>

        <?php
    }


    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['devsales_user'] = strip_tags($new_instance['devsales_user']);
        $instance['devsales_apikey'] = strip_tags($new_instance['devsales_apikey']);
        $instance['devsales_product'] = strip_tags($new_instance['devsales_product']);
        $instance['devsales_days'] = strip_tags($new_instance['devsales_days']);
        return $instance;
    }


    function widget($args, $instance) {
        extract($args, EXTR_SKIP);

        echo $before_widget;
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
        $user = esc_attr($instance['devsales_user']);
        $apikey = esc_attr($instance['devsales_apikey']);

        $product = esc_attr($instance['devsales_product']);
        $product = ucwords(strtolower($product));

        $days = esc_attr($instance['devsales_days']);


        if (!empty($title))
            echo $before_title . $title . $after_title;

        $apiurl = sprintf('http://api.pagelines.com/devs/1/%s/%s/%s/sales/%s',$user,$apikey,$product,$days);

        $transientKey = "DevSalesWidgetSales-".$product;

        $cached = get_transient($transientKey);

        if (false !== $cached) {
            return $cached;
        }

        $remote = wp_remote_get(esc_url($apiurl));

        if( is_wp_error( $remote ) ) {
            echo '<p>There was an error getting the data. Please try again later.</p>';
        }

        $data = json_decode( $remote['body'],true );

        //var_dump(json_decode($remote['body'],true));

        $total = $data["total"];
        $cut = $total * .30;
        $takehome = $total - $cut;

        if($days == '') {

            $item = sprintf('%s has made $%s',$product,$takehome);

        } else {

            $item = sprintf('%s has made $%s in the last %s days',$product, $takehome, $days);

        }

        $output = sprintf('<div class="ba-devsales-widget"><div class="ba-devsales-widget-pad">%s</div></div>',$item);

        set_transient($transientKey, $output, 600);

        echo $output;

        echo $after_widget;
    }

}